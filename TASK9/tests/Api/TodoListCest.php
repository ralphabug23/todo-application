<?php


namespace Tests\Api;

use Tests\Support\ApiTester;

class TodoListCest
{ 
    public function iShouldUpdate(ApiTester $I)
    {
      

            $I->haveHttpHeader('Content-Type', 'application/json');
                $I->sendPUT('http://localhost/OJT%20TASK/API.php', [
                     // 'id'=> input valid id//
                    'task_title' =>'task_title',
                    'task_name' => 'task_name',
                    'time' => 'time'
                    ]);

                $I->seeResponseCodeIs(200);
                $I->seeResponseIsJson();
                $I->seeResponseContainsJson(['status' => 'success']);
        

    }
    public function iShouldInsert(ApiTester $I)
    {

            $I->haveHttpHeader('Content-Type', 'application/json');
                $I->sendpost('http://localhost/OJT%20TASK/API.php', [
                    'task_title' =>'task_title',
                    'task_name' => 'task_name',
                    'time' => 'time'
                    ]);

                $I->seeResponseCodeIs(200);
                $I->seeResponseIsJson();
                $I->seeResponseContainsJson(['status' => 'success']);
        

    }
    public function iShouldRead(ApiTester $I)
    {
      
            $I->haveHttpHeader('Content-Type', 'application/json');
                $I->sendGET('http://localhost/OJT%20TASK/API.php', [
                    'task_title' =>'task_title',
                    'task_name' => 'task_name',
                    'time' => 'time'
                    ]);

                $I->seeResponseCodeIs(200);
                $I->seeResponseIsJson();
                $I->seeResponseContainsJson(['status' => 'success']);
        

    }
    public function iShouldDelete(ApiTester $I)
{
  

        $I->haveHttpHeader('Content-Type', 'application/json');
            $I->sendDELETE('http://localhost/OJT%20TASK/API.php', [
               // "id" => input valid id//
                'task_title' =>'task_title',
                'task_name' => 'task_name',
                'time' => 'time'
                ]);

            $I->seeResponseCodeIs(200);
            $I->seeResponseIsJson();
            $I->seeResponseContainsJson(['status' => 'success']);
    

}
}

