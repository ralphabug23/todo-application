<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type");

require_once "MysqliDb.php";


class API {

   
    public function __construct()
    {
       $this->db = new MysqliDb('localhost', 'root', '' ,'todolist' );  
    }

    //GET/READ function
    public function httpGet($progress)
    {
        $progress = $this->db->get('tbl_to_do_list', NULL,'*' );
        if($progress)
        {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'success',
                'data' => $progress
            ));

        }else
        {

            echo json_encode(array(
                'method' => 'GET',
                'status' => 'failed',
                'message' =>  "failed fetch request"
            ));
        }
            json_encode($progress);

    }

    //POST/INSERT Function 
    public function httpPost($insert)
    {      
            $data = Array (
                "task_title" => $insert["task_title"],
                "task_name" => $insert["task_name"],
                "time" => $insert["time"]
            ); 

                $task = $this->db->insert('tbl_to_do_list', $data );
                if($task){

                    $response = array(  
                        'method' => 'POST',
                        'status' => 'success',
                        'data' => $task
                    );
                    http_response_code(200);

                    echo json_encode($response);
                }else{
                    http_response_code(400);

                    echo json_encode(array(
                        'method' => 'POST',
                        'status' => 'failed',
                        'message' => 'failed to insert Data'
                    ));
                }
                json_encode($data);
            }
            


    //update/PUT function
    public function httpPut($id,$update)
    {
       
            $id = $update['id'];
            $existingRecord = $this->db->where('id', $id)->getOne('tbl_to_do_list');
            $data = Array(
                "task_title" => $update["task_title"],
                "task_name" => $update["task_name"],
                "time" => $update["time"]
            );
            if (!$existingRecord) 
            {
                http_response_code(400);
                echo json_encode(array(
                    'method' => 'PUT',
                    'status' => 'failed',
                    'message' => 'Record with the provided ID does not exist'
                ));

            }
            else
            {
                
                    http_response_code(200);

                    echo json_encode(array(
                        'method' => 'PUT',
                        'status' => 'success',
                        'data' => $data
                    ));  
                }
             
                       $this->db->where("id", $id);
                        $updated=$this->db->update('tbl_to_do_list', $data);
                        
                             
                        
                        json_encode($updated);
    }
    

    

    //DELETE/DELETE Function
    public function httpDelete($id, $delete)
    {

        $id = $delete['id'];
        
        $existingRecord = $this->db->where('id', $id)->getOne('tbl_to_do_list');
        if (!$existingRecord) 
        {
            http_response_code(400);
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'Record with the provided ID does not exist'
            ));

        }
        else
            {
            
                http_response_code(200);

                echo json_encode(array(
                    'method' => 'PUT',
                    'status' => 'success',
                    'data' => $delete
                ));  
            }
           
            $this->db->where('id', $id);
            $this->db->delete('tbl_to_do_list');
          
       
            
            json_encode($delete);
          
    }

}

$request_method = $_SERVER['REQUEST_METHOD'];
  // For GET,POST,PUT & DELETE Request
  if ($request_method === 'GET') {
    $received_data = $_GET;
} else {
    //check if method is PUT or DELETE, and get the ids on URL
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];


        $ids = null;

        $exploded_request_uri = array_values(explode("/", $request_uri));


        $last_index = count($exploded_request_uri) - 1;


        $ids = $exploded_request_uri[$last_index];


        }
    }

    //payload data
    $received_data = json_decode(file_get_contents('php://input'), true);


$api = new API;


 //Checking if what type of request and designating to specific functions
  switch ($request_method) {
      case 'GET':
          $api->httpGet($received_data);
          break;
      case 'POST':
          $api->httpPost($received_data);
          break;
      case 'PUT':
          $api->httpPut($ids, $received_data);
          break;
      case 'DELETE':
          $api->httpDelete($ids, $received_data);
          break;
  }

?>
