<?php
require_once "MysqliDb.php";


$mysqli = new mysqli('localhost', 'root', '', 'task8');
$db = new MysqliDb($mysqli);

if(isset($_GET['id'])){
    $id = $_GET['id'];

    $db->where('id', $id);
    $success = $db->delete('employee');

    
    if($success){
        header("location: submitted.php");
        echo "Deleted successfully";
    } else {
        echo "Error deleting record: " . $db->getLastError();
    }
}
?>
