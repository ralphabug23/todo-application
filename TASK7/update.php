<?php

include "config.php";

$conn = new mysqli($server, $username, $password, "employee");

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$firstName = $middleName = $lastName = $birthday = $address = $users_id = "";

if(isset($_POST['update'])){
    $firstName = $_POST["fname"];   
    $middleName = $_POST["mname"];
    $lastName = $_POST["lname"];
    $birthday = $_POST['birthday'];
    $address = $_POST["address"];
    $users_id = $_POST['id'];

    $sql = "UPDATE user SET first_name = '$firstName', middle_name = '$middleName', last_name = '$lastName', birthday = '$birthday', address = '$address' WHERE id = $users_id";
    $result = $conn->query($sql);

    if($conn->affected_rows > 0){
        echo "Updated the data";
    }else{
        echo "Error: " . $conn->error;
    }
}

if(isset($_GET['id'])){
    $users_id = $_GET['id'];

    $sql = "SELECT * FROM user WHERE id = $users_id";
    $result = $conn->query($sql);

    if($result->num_rows > 0){
        $row = $result->fetch_assoc();
        $firstName = $row['first_name'];
        $middleName = $row['middle_name'];
        $lastName = $row['last_name'];
        $birthday = $row['birthday'];
        $address = $row['address'];
        $users_id = $row['id'];
    }
}

$conn->close();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update User</title>
</head>
<body>
    <form action="" method="POST">
    <fieldset>
    <legend>Personal Info</legend>
    <input type="hidden" name="id" value="<?php echo $users_id; ?>">
    <label for="fname">First Name:</label>
    <input type="text" name="fname" value="<?php echo $firstName; ?>">
    <br>
    <label for="mname">Middle Name:</label>
    <input type="text" name="mname" value="<?php echo $middleName; ?>">
    <br>
    <label for="lname">Last Name:</label>
    <input type="text" name="lname" value="<?php echo $lastName; ?>">
    <br>
    <label for="birthday">Birthday:</label>
    <input type="text" name="birthday" value="<?php echo $birthday; ?>">
    <br>
    <label for="address">Address:</label>
    <input type="text" name="address" value="<?php echo $address; ?>">
    <br>
    <input type="submit" value="Update" name="update">
    </fieldset>
    </form>
</body>
</html>
