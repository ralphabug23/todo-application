<?php

include "config.php";

$conn = new mysqli($server, $username, $password, "employee");

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if(isset($_POST["create"])){
    $firstname = $_POST['fname'];
    $middlename = $_POST['mname'];
    $lastname =  $_POST['lname'];
    $birthday = $_POST['birthday'];
    $address = $_POST['address'];

    $sql = "INSERT INTO user (first_name, middle_name, last_name, birthday, address) 
            VALUES ('$firstname', '$middlename', '$lastname', '$birthday', '$address')";

    if($conn->query($sql) === TRUE) {
        echo "Record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

$conn->close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post">
        <label for="fname">First Name:</label>
        <input type="text" name="fname">
        <br>
        <label for="lname">Last Name:</label>
        <input type="text" name="lname">
        <br>
        <label for="mname">Middle Name:</label>
        <input type="text" name="mname">
        <br>
        <label for="birthday">Birthday:</label>
        <input type="text" name="birthday">
        <br>
        <label for="address">Address:</label>
        <input type="text" name="address">
        <br>
        <input type="submit" name="create" value="Create">
    </form>
</body>
</html>
