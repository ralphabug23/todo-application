<?php

include "config.php";

$conn-> SELECT_DB('employee');

$sql = "SELECT * FROM user";
$result = $conn->query($sql);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <table>
        <td>
            <th>ID</th>
            <th>Firstname</th>
            <th>Middlename</th>
            <th>Lastname</th>
            <th>birthday</th>
            <th>Address</th>
            <th>action</th>
        </td>
 <tbody>
 <?php
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        ?>
        <tr>
            <td><?php echo $row['id']; ?></td>
            <td><?php echo $row['first_name']; ?></td>
            <td><?php echo $row['middle_name']; ?></td>
            <td><?php echo $row['last_name']; ?></td>
            <td><?php echo $row['birthday']?></td>
            <td><?php echo $row['address']?></td>
            <td>
                <a class="updateBTN" href="update.php?id=<?php echo $row['id']; ?>">Edit</a>&nbsp;
                <a class="deleteBTN" href="delete.php?id=<?php echo $row['id']; ?>">Delete</a>
            </td>
        </tr>
        <?php
    }
}
?>

            </tbody>
        </table>
</body>
</html>