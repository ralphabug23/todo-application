<?php

$host = 'localhost';
$username = 'your_username';
$password = 'your_password';
$database = 'your_database_name';


$conn = new mysqli($host, $username, $password, $database);


if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}else{
    echo "Connected Successfully". $conn_error;
}

$sql = "CREATE TABLE `employee`  (
    `id` int NOT NULL AUTO_INCREMENT,
    `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `middle_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `birthday` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
  ) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;
  
  SET FOREIGN_KEY_CHECKS = 1";

// Insert data into the employee table
$sql = "CREATE TABLE Employee";
if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}




// retrieve the first name, last name, and birthday of all employees in the table
$sql =  "SELECT Firstname, Lastname, Birthday" ;
$result = $conn->query($sql);


if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["Firstname"]. " - Last Name: " . $row["Lastname"]. " - Birthday: " . $row["Birthday"]. "<br>";
    }
} else {
    echo "0 results";
}


// retrieve the number of employees whose last name starts with the letter 'S'
$sql =  $sql = "SELECT * FROM employee WHERE Lastname like 'D%' ORDER BY reg_date DESC"  ;
$result = $conn->query($sql);


if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "id: .$row[id].   First Name: " . $row["firstname"]. " - Last Name: " . $row["lastname"]. " - Birthday: " . $row["Birthday"]. "<br>";
    }
} else {
    echo "0 results";
}


//  retrieve the first name, last name, and address of the employee with the highest ID number

$sql = "SELECT Firstname, Lastname, adress, MAX(id) AS max_id FROM employee";
$result = $conn->query($sql);


if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["Firstname"]. " - Last Name: " . $row["Lastname"]. " - Address: " . $row["adress"]. "<br>";
    }
} else {
    echo "0 results";
}


// Update data in the employee table
$sql =  "UPDATE employee SET Lastname = 'Draken' WHERE id = 1";
if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully";
} else {
    echo "Error updating record: " . $conn->error;
}


// Delete data from the employee table
$sql = "DROP TABLE employee";
if ($conn->query($sql) === TRUE) {
    echo "Record deleted successfully";
} else {
    echo "Error deleting record: " . $conn->error;
}
$conn->close();


?>
